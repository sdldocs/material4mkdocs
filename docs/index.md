# Technical documentation that just works

Markdown 파일로부터 브랜드 정적 사이트를 만들어 오픈 소스 또는 상용 프로젝트의 문서를 호스팅할 수 있다. 5분 만에 구축할 수 있다.

- [Quick start](getting-started/index.md)
- [Get Insiders](under-construction.md)
