# 대안
많은 종류의 정적인 사이트 생성기와 테마가 있다.테크놀로지 스택에 적합한 것을 선택하는 것은 쉽지 않은 결정이다. MkDocs에 적합한 솔루션인지 확실하지 않은 경우 이 섹션을 통해 대체 솔루션을 평가할 수 있을 것이다.

## Docusaurus <a id="docusaurus"></a>
Facebook의 [Docusaurus](https://docusaurus.io/)는 매우 인기 있는 문서 생성기이며, 귀하 또는 귀하의 회사가 이미 사이트를 구축하기 위해 [React](https://reactjs.org/)를 사용하고 있다면 좋은 선택일 수 있다. Material for MkDocs가 생성하는 사이트와는 근본적으로 다른 [single page application](https://en.wikipedia.org/wiki/Single-page_application)을 생성한다.

__장점__
- 매우 강력하고, 커스터마이즈 가능하며, 확장 가능하다.
- 기술 문서 작성에 도움이 되는 많은 컴포넌트를 제공한다.
- Facebook이 지원하는 크고 풍부한 생태계.

__단점__
- 높은 학습 곡선, JavaScript 지식 필수.
- JavaScript 생태계는 매우 불안정하며 유지보수가 상당히 어려움.
- 도입 및 가동에 더 많은 시간 필요.

[Docusaurus](https://docusaurus.io/)는 단일 페이지 어플리케이션을 출력하는 문서 사이트에서는 최적의 선택지 중 하나이지만, [Docz](https://www.docz.site/), [Gatsby](https://www.gatsbyjs.com/), [Vuepress](https://vuepress.vuejs.org/), [Docsify](https://docsify.js.org/) 등의 솔루션에서 이 문제에 비슷하게 접근하고 있다. 

## Jekyll <a id="jakyll"></a>
[Jekyll]()은 아마도 가장 성숙하고 널리 퍼진 정적 사이트 생성기 중 하나이며 [Ruby]()로 작성되었다. 기술적인 프로젝트의 문서화를 목적으로 하는 것은 아니고, 선택할 수 있는 테마가 많기 때문에 어려울 점이 있을 수 있다.

__장점__
- 다양한 테마를 선택할 수 있는 풍부한 에코시스템.
- 블로그에 뛰어난 기능 제공 (permalinks, 태그 등).
- Material for MkDocs와 유사한 SEO 친화적 사이트 생성.

__단점__
- 기술 프로젝트 문서화를 위해 특별히 설계된 것이 아님.
- Python Markdown만큼 고급적이지 않은 제한된 Markdown 기능
- 도입 및 가동에 더 많은 시간 필요.

## Sphinx <a id="sphinx"></a>
[Sphinx]()는 레퍼런스 문서 생성에 특화된 대체 정적 사이트 생성기로, MkDocs에는 없는 강력한 기능을 제공한다. Markdown과 유사한 형식인 [reStructured text]()를 사용한다.사용자에 따라 다르겠지만 일반적으로 사용하기 쉽지 않다.

__장점__
- 매우 강력하고, 커스터마이즈 가능하며, 확장 가능하다.
- Python Docstrings에서 참조 문서를 생성한다.
- 많은 Python 프로젝트에서 사용되는 크고 풍부한 생태계

__단점__
- 높은 학습 곡선, reStructured text 구문은 어려울 수 있다.
- 검색 성능이 MkDocs에서 제공하는 것과 비교하여 떨어진다.
- 도입 및 가동에 더 많은 시간 필요.

[Sphinx Immaterial](https://github.com/jbms/sphinx-immaterial)은 Material for MkDocs에서 Sphinx로 가는 훌륭한 관문이다.그것은 업스트림 저장소와 밀접하게 관련되어 있기 때문이다. [Furo](https://pradyunsg.me/furo/)는 또 하나의 훌륭한 테마이다. 또는 MkDocs 위에 [mkdocstring](https://github.com/mkdocstrings/mkdocstrings)을 구축하여 참조 문서를 생성할 수도 있다.

## GitBook <a id="gitbook"></a>
[GitBook](https://www.gitbook.com/)은 GitHub 저장소의 Markdown 파일에서 아름답고 기능적인 사이트를 생성하는 호스팅된 문서 솔루션을 제공한다. 그러나 한때 오픈 소스였지만 얼마 전에는 클로즈드 소스 솔루션으로 바뀌었다.

__장점__
- 호스트형 솔루션, 최소한의 기술 지식 필요.
- 커스텀 도메인, 인증 및 기타 엔터프라이즈 기능.
- 협업을 위한 뛰어난 콜라보레이션 기능.

__단점__
- 비공개 소스, 독점 프로젝트용 무료 제공 안 됨.
- Python Markdown만큼 다양하지 않못한 제한된 Markdown 기능
- 많은 오픈 소스 프로젝트들이 GitBook을 떠남.

많은 사용자들이 [GitBook]()에서 material for MkDocs으로 전환했다.이는 오픈 소스 솔루션을 선호하기 때문이다.

