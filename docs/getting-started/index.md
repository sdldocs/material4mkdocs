# 시작하기
Material for MkDocs은 프로젝트 기술 문서화를 지향하는 정적 사이트 생성기인 MkDocs의 테마이다. Python에 대해 잘 알고 있다면 Python 패키지 관리자 pip 명령으로 MkDocs용 Material을 설치할 수 있다. 그렇지 않다면 도커를 사용하는 것을 추천한다.

- [설치](installation.md)
- [사이트 생성](creating-your-site.md)
- [사이트 퍼블리싱](publishing-your-site.md)
- [커스터마이제이션](customization.md)
- [브라우저 지원](browser-support.md)
- [대안](alternatives.md)
