# 사이트 퍼블리싱 
<code>git</code> 저장소에 프로젝트 문서를 호스팅하는 가장 큰 장점은 새로운 변경이 푸시될 때 자동으로 문서를 배포할 수 있다는 점다. MkDocs는 이것을 매우 단순하게 만든다.

## GitHub Pages <a id="github-pages"></a>
이미 GitHub에서 코드를 호스팅하고 있다면 [GitHub Pages](https://pages.github.com/)가 프로젝트 문서를 게시하는 가장 편리한 방법일 것이다. 무료이며 설치하기도 쉽기 떄문이다.

### GitHub Actions 으로 <a id="with-github-actions"></a>
[GitHub Actions](https://github.com/features/actions)를 사용하면 프로젝트 문서 배포를 자동화할 수 있습. 저장소의 루트에서 새로운 GitHub Actions 워크플로(예: .github/workflows/ci.yml)를 만들고 다음 내용을 복사하여 붙여 넣는다.

=== "Material for MkDocs"

``` yaml
name: ci # (1)!
on:
  push:
    branches:
      - master # (2)!
      - main
jobs:
  deploy:
    runs-on: ubuntu-latest
    steps:
      - uses: actions/checkout@v2
      - uses: actions/setup-python@v2
        with:
          python-version: 3.x
      - run: pip install mkdocs-material # (3)!
      - run: mkdocs gh-deploy --force
```

1.  You can change the name to your liking. 

2.  At some point, GitHub renamed `master` to `main`. If your default branch
    is named `master`, you can safely remove `main`, vice versa.

3.  This is the place to install further [MkDocs plugins] or Markdown
    extensions with `pip` to be used during the build:

    ``` sh
    pip install \
      mkdocs-material \
      mkdocs-awesome-pages-plugin \
      ...
    ```
=== "Insiders"

``` yaml
name: ci
on:
  push:
    branches:
      - master
      - main
jobs:
  deploy:
    runs-on: ubuntu-latest
    if: github.event.repository.fork == false
    steps:
      - uses: actions/checkout@v2
      - uses: actions/setup-python@v2
        with:
          python-version: 3.x
      - run: pip install git+https://${GH_TOKEN}@github.com/squidfunk/mkdocs-material-insiders.git
      - run: mkdocs gh-deploy --force
env:
  GH_TOKEN: ${{ secrets.GH_TOKEN }} # (1)!
```

1.  Remember to set the `GH_TOKEN` environment variable to the value of your
    [personal access token] when deploying [Insiders], which can be done
    using [GitHub secrets].

이제 새로운 커밋이 <code>master</code> 또는 <code>main</code> 분기 중 하나에 푸시되면 정적 사이트가 자동으로 구축되어 배포된다. 변경 내용을 누르면 워크플로우가 작동 중인지 확인할 수 있다.

몇 분이 지나도 GitHub Page가 나타나지 않으면 저장소 설정으로 이동하여 GitHub Page의 [publising source branch](https://docs.github.com/en/pages/getting-started-with-github-pages/configuring-a-publishing-source-for-your-github-pages-site)가 <code>gh-pages</code>로 설정되어 있는지 확인하여야 한다.

생성된 문서는 <code>\<username\>.github.io/\<repository\>></code>에서 볼 수 있다.

### MkDocs로 <a id="with-mkdocs"></a>
프로젝트 문서를 수동으로 배포하려면 <code>mkdocs.yml</code> 파일이 포함된 디렉토리에서 다음 명령을 호출하면 된다.

```
mkdocs gh-deploy --force
```

## GitLab Pages <a id="gitlab-pages"></a>
GitLab에서 코드를 호스팅하는 경우 [GitLab CI](https://docs.gitlab.com/ee/ci/) 태스크 러너를 사용하여 [GitLab Pages](https://gitlab.com/pages)로 배포할 수 있다. 저장소 루트에 <code>.gitlab-ci.yml</code>이라는 작업 정의를 만들고 다음 내용을 복사하여 붙여넣는다.

=== "Material for MkDocs"

``` yaml
image: python:latest
pages:
  stage: deploy
  only:
    - master # (1)!
    - main
  script:
    - pip install mkdocs-material
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
```

1.  At some point, GitLab renamed `master` to `main`. If your default branch
    is named `master`, you can safely remove `main`, vice versa.
=== "Insiders"

``` yaml
image: python:latest
pages:
  stage: deploy
  only:
    - master
    - main
  script: # (1)!
    - pip install git+https://${GH_TOKEN}@github.com/squidfunk/mkdocs-material-insiders.git
    - mkdocs build --site-dir public
  artifacts:
    paths:
      - public
```

1.  Remember to set the `GH_TOKEN` environment variable to the value of your
    [personal access token] when deploying [Insiders], which can be done
    using [masked custom variables].

이제 새로운 커밋이 `마스터`로 푸시되면 정적 사이트가 자동으로 구축되어 배포된니. 워크플로우의 동작을 확인하려면 파일을 저장소에 커밋하고 푸시한다.

생성된 문서는 `<username\.gitlab.io/\<repository>`에서 볼 수 있다.
