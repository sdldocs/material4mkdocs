# 설치

### pip으로 설치 (추천)<a id="with-pip"></a> 
<code>pip</code>으로 Material for MkDocs를 설치할 수 있다. 

```
pip install mkdocs-material
```

모든 종속성의 호환 버전 즉, [MkDocs](https://www.mkdocs.org/), [Markdown](https://python-markdown.github.io/), [Pygments](https://pygments.org/) 및 [Python Markdown Extension](https://facelessuser.github.io/pymdown-extensions/)이 자동으로 설치된다. Material for MkDocs는 항상 최신 버전을 지원하기 위해 노력하므로 이러한 패키지를 별도로 설치할 필요는 없다.

> Note <br>
> 현재 Material for MkDocs이 추천하는 설치 방법만을 위와 같이 가술하였다. 다른 기술 방법이 필요하면 Material for MkDocs 사이트의 [설치](https://squidfunk.github.io/mkdocs-material/getting-started/#installation)를 참조 한다.

> - [docker로 설치](https://squidfunk.github.io/mkdocs-material/getting-started/#with-docker) 

> - [git으로 설치](https://squidfunk.github.io/mkdocs-material/getting-started/#with-git)
