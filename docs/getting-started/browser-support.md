# 브라우저 지원
Material for MkDocs는 [custom properties](https://caniuse.com/css-variables)이나 [mask images](https://caniuse.com/mdn-css_properties_mask-image) 등의 최신 CSS 기능을 통해 커스터마이즈할 수 있는 가장 심플한 가능성을 유지하면서 가능한 한 다양한 브라우저를 지원한고 있다.

## 지원되는 브라우저 <a id="supported-browsers"></a>
다음 표는 Material for MkDocs이 완전히 지원되는 모든 브라우저를 나열한 것이다. 따라서 모든 기능이 성능 저하 없이 작동한다고 가정할 수 있다. 지원되는 버전 범위의 브라우저에서 기능을 사용할 수 없는 경우 [이슈를 제가하도록 한다](https://github.com/squidfunk/mkdocs-material/issues/new/choose).

<figure markdown>

| Browser                              | Version | Release date |         |        |      Usage |
| ------------------------------------ | ------: | -----------: | ------: | -----: | ---------: |
|                                      |         |              | desktop | mobile |    overall |
| :fontawesome-brands-chrome: Chrome   |     49+ |      03/2016 | 25.65%  | 38.33% |     63.98% |
| :fontawesome-brands-safari: Safari   |     10+ |      09/2016 |  4.63%  | 14.96% |     19.59% |
| :fontawesome-brands-edge: Edge       |     79+ |      01/2020 |  3.95%  |    n/a |      3.95% |
| :fontawesome-brands-firefox: Firefox |     53+ |      04/2017 |  3.40%  |   .30% |      3.70% |
| :fontawesome-brands-opera: Opera     |     36+ |      03/2016 |  1.44%  |   .01% |      1.45% |
|                                      |         |              |         |        | __92.67%__ |

  <figcaption markdown>

Browser support matrix sourced from caniuse.com.

  </figcaption>
</figure>

사용률 데이터는 전 세계 브라우저 시장 점유율을 기반으로 하기 때문에 실제로 대상 인구에 따라 완전히 다를 수 있다. 사용자 간의 브라우저 유형과 버전 분포를 확인하는 것이 필요하다.

## 그외 브라우저 <a id="other-browsers"></a>
사이트가 최신 브라우저로 볼 때처럼 완벽하지 않을 수 있지만 다음과 같은 이전 버전의 브라우저에는 약간의 추가 작업이 필요할 수 있다.

- :fontawesome-brands-firefox: __Firefox 31-52__ – icons will render as little
  boxes due to missing support for [mask images]. While this cannot be
  polyfilled, it might be mitigated by hiding icons with [additional CSS].
- :fontawesome-brands-edge: __Edge 16-18__ – the spacing of some elements might be
  a little of due to missing support for the [:is pseudo selector], which can be
  mitigated with some [additional CSS].
- :fontawesome-brands-internet-explorer: __Internet Explorer__ - no support,
  mainly due to missing support for [custom properties]. The last version of
  Material for MkDocs to support Internet Explorer is
  [:octicons-tag-24: 4.6.3][IE support].
