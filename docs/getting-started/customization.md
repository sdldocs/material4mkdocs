# 커스터마이제이션
프로젝트 문서화는 프로젝트 자체만큼이나 다양하며 Material for MkDocs는 아름답게 보이기 위한 훌륭한 출발점이다. 그러나 문서를 작성할 때 브랜드 스타일을 유지하기 위해 약간의 조정이 필요할 수 있다.

## 애쎗 추가 <a id="adding-assets"></a>
[MkDocs](https://www.mkdocs.org/)는 테마를 커스터마이즈할 수 있는 몇 가지 방법을 제공한다. Material for MkDocs을 약간 수정하려면 `docs` 디렉토리에 CSS 및 JavaScript 파일을 추가한다.

### 추가 CSS <a id="additional-css"></a>
일부 색상을 조정하거나 특정 요소의 간격을 변경하려면 별도의 스타일시트로 변경할 수 있다. 가장 쉬운 방법은 `docs` 디렉토리에 새로운 스타일시트 파일을 작성하는 것이다.

```
.
├─ docs/
│  └─ stylesheets/
│     └─ extra.css
└─ mkdocs.yml
```

그런 다음 `mkdocs.yml`에 다음 행을 추가한다.

```
extra_css:
  - stylesheets/extra.css
```

### 추가 JavaScript <a id="additional-javascript"></a>
별도의 강조 기능 구문을 통합하거나 테마에 사용자 로직을 추가할 경우 `docs` 디렉토리에 새 JavaScript 파일을 만든다.

```
.
├─ docs/
│  └─ javascripts/
│     └─ extra.js
└─ mkdocs.yml
```

그런 다음 `mkdocs.yml`에 다음 행을 추가한다.
```
extra_javascript:
  - javascripts/extra.js
```

## 테미 확장 <a id="extending-theme"></a>
HTML 소스(예: 일부 부분을 추가 또는 제거)를 변경하려는 경우 테마를 확장할 수 있다. MkDocs는 [테마 확장](https://www.mkdocs.org/user-guide/styling-your-docs/#using-the-theme-custom_dir)을 지원하며 git에서 포킹하지 않고도 material for MkDocs의 일부를 덮어쓸 수 있는 쉬운 방법이다. 이를 통해 최신 버전으로 보다 쉽게 업데이트할 수 있다.

### 설정 및 테마 구조 <a id="setup-and-theme-structure"></a>
`mkdocs.yml`에서 통상대로 Material for MkDocs을 활성화한 후 `custom_dir` 설정을 사용하여 참조할 오버라이드의 새 폴더를 만든다.

```
theme:
  name: material
  custom_dir: overrides
```

!!! warning "Theme extension prerequisites"

    As the [`custom_dir`][custom_dir] setting is used for the theme extension
    process, Material for MkDocs needs to be installed via `pip` and referenced
    with the [`name`][name] setting in `mkdocs.yml`. It will not work when
    cloning from `git`.

`overrides` 디렉토리의 파일은 원래 테마의 일부로서 동일한 이름으로 대체되므로 `overrides` 디렉토리 구조는 원래 테마의 디렉토리 구조를 미러링해야 한다. 또한 추가 자산은 `overrides` 디렉토리에 저장될 수도 있다.

```
.
├─ .icons/                             # Bundled icon sets
├─ assets/
│  ├─ images/                          # Images and icons
│  ├─ javascripts/                     # JavaScript files
│  └─ stylesheets/                     # Style sheets
├─ partials/
│  ├─ integrations/                    # Third-party integrations
│  │  ├─ analytics/                    # Analytics integrations
│  │  └─ analytics.html                # Analytics setup
│  ├─ languages/                       # Translation languages
│  ├─ content.html                     # Page content
│  ├─ copyright.html                   # Copyright and theme information
│  ├─ footer.html                      # Footer bar
│  ├─ header.html                      # Header bar
│  ├─ language.html                    # Translation setup
│  ├─ logo.html                        # Logo in header and sidebar
│  ├─ nav.html                         # Main navigation
│  ├─ nav-item.html                    # Main navigation item
│  ├─ palette.html                     # Color palette
│  ├─ search.html                      # Search interface
│  ├─ social.html                      # Social links
│  ├─ source.html                      # Repository information
│  ├─ source-file.html                 # Source file information
│  ├─ tabs.html                        # Tabs navigation
│  ├─ tabs-item.html                   # Tabs navigation item
│  ├─ toc.html                         # Table of contents
│  └─ toc-item.html                    # Table of contents item
├─ 404.html                            # 404 error page
├─ base.html                           # Base template
└─ main.html                           # Default page
```

### 일부 덮어쓰기 <a id="overriding"></a>
일부를 덮어쓰기 위해 `override` 디렉토리내의 동일한 이름과 위치를 가진 파일로 대체할 수 있다. 예를 들어 원래 footer.html partial를 대체하려면 `override` 디렉토리에 새 `footer.html` partial을 만든다.
```
.
├─ overrides/
│  └─ partials/
│     └─ footer.html
└─ mkdocs.yml
```

이제 MkDocs는 테마를 렌더링할 때 새로운 파일을 사용한다. 이 작업은 모든 파일에서 적용할 수 있다.

### 블럭 덮어쓰기 <small>recommended</small> <a id="overriding-blocks"></a>
일부 덮어쓰기외에도 템플릿 내부에 정의되어 특정 기능을 래핑하는 템플릿 블록을 덮어쓰기(및 확장)를 할 수도 있다. 블록 덮어쓰기를 설정하려면 `overrides` 디렉토리 내에 main.html 파일을 만든다.
```
.
├─ overrides/
│  └─ main.html
└─ mkdocs.yml
```

그런 다음 예를 들어 사이트 제목을 덮어쓰려면 `main.html`에 다음 행을 추가한다.
```jinja
{% extends "base.html" %}

{% block htmltitle %}
  <title>Lorem ipsum dolor sit amet</title>
{% endblock %}
```

테마별로 제공되는 템플릿 블록은 다음과 같다.

| 블럭 이름           | 목적                                             |
| :---------------- | :---------------------------------------------- |
| `analytics`       | Wraps the Google Analytics integration          |
| `announce`        | Wraps the announcement bar                      |
| `config`          | Wraps the JavaScript application config         |
| `content`         | Wraps the main content                          |
| `extrahead`       | Empty block to add custom meta tags             |
| `fonts`           | Wraps the font definitions                      |
| `footer`          | Wraps the footer with navigation and copyright  |
| `header`          | Wraps the fixed header bar                      |
| `hero`            | Wraps the hero teaser (if available)            |
| `htmltitle`       | Wraps the `<title>` tag                         |
| `libs`            | Wraps the JavaScript libraries (header)         |
| `outdated`        | Wraps the version warning                       |
| `scripts`         | Wraps the JavaScript application (footer)       |
| `site_meta`       | Wraps the meta tags in the document head        |
| `site_nav`        | Wraps the site navigation and table of contents |
| `styles`          | Wraps the style sheets (also extra sources)     |
| `tabs`            | Wraps the tabs navigation (if available)        |


## 테마 개발 <a id="theme-development"></a>
Material for MkDocs는 [TypeScript](https://www.typescriptlang.org/), [RxJ](https://github.com/ReactiveX/rxjs) 및 [SAS](https://sass-lang.com/)를 기반으로 구축되며, 모든 것을 정리하기 위해 린 커스텀 빌드 프로세스를 사용한다. 보다 근본적인 변경을 원하는 경우 테마의 소스를 직접 변경하여 컴파일해야 할 수도 있다.

### 환경 설정 <a id="environment-setup"></a>
Material for MkDocs를 시작하려면 [Node.js](https://nodejs.org/) 버전 14 이상이 필요하다. 먼저 저장소를 복제한다.
```
git clone https://github.com/squidfunk/mkdocs-material
```

다음으로, 모든 종속 관계를 설치할 필요가 있다. 이 작업은 다음과 같다.
```
cd mkdocs-material
pip install -e .
pip install mkdocs-minify-plugin
pip install mkdocs-redirects
npm install
```

### 개발 모드 <a id="development-mode"></a>
워처(watcher)를 시작한다.
```
npm watcher
```

그런 다음 두 번째 터미널 창에서 MkDocs 라이브 미리보기 서버를 시작한다.
```
mkdocs serve --watch-theme
```

브라우저에서 주소창에 `localhost:8000`을 입력하면 바로 그 문서가 나타난다.
Material for MkDocs 개발을 시작하려면 Node.js 버전이 14개 이상 필요합니다. 먼저 저장소를 복제합니다.

!!! warning "Automatically generated files"

    Never make any changes in the `material` directory, as the contents of this
    directory are automatically generated from the `src` directory and will be
    overwritten when the theme is built.

  [live preview]: http://localhost:8000

### 테마 빌드 <a id="building-the-theme"></a>
변경이 완료되면 다음과 같이 수행하여 테마를 작성할 수 있다.
```
npm run build
```

이를 통해 모든 스타일시트와 JavaScript 파일에 대한 프로덕션 수준 컴파일과 최소화를 트리거한다. 명령 수행이 종료되면 컴파일된 파일은 `material` 디렉토리에 저장된다. `mkdocs build`를 실행하면 원래 테마에 대한 변경 내용이 보여진다.
