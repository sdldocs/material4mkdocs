
[comment]: # (---)
[comment]: # (template: overrides/main.html)
[comment]: # (---)
[comment]: # (This actually is the most platform independent comment)

# 사이트 생성
Material for MkDocs를 설치한 다음, <code>mkdocs</code> 실행 파일을 사용하여 프로젝트 문서를 부트스트랩할 수 있다. 프로젝트를 배치할 디렉터리로 이동한 후 다음을 입력한다.

```
mkdocs new .
```



다음과 같은 구조가 만들어 질 것이다.

```
.
├─ docs/
│  └─ index.md
└─ mkdocs.yml
```

## 설정 <a id="configuration"></a>
### 최소 설정 <a id="minimal-configuration"></a>
다음 줄을 <code>mkdocs.yml</code>에 추가하여 테마를 사용 가능하도록 설정한다. 여러 가지 설치 방법이 있지만 최소 설정은 약간 다를 수 있다.

```
theme:
    name: material
```

???+ tip "Recommended: [configuration validation and auto-complete]"

    In order to minimize friction and maximize productivity, Material for MkDocs 
    provides its own [schema.json][^1] for `mkdocs.yml`. If your editor supports
    YAML schema validation, it's definitely recommended to set it up:

    === "Visual Studio Code"

        1.  Install [`vscode-yaml`][vscode-yaml] for YAML language support.
        2.  Add the schema under the `yaml.schemas` key in your user or
            workspace [`settings.json`][settings.json]:

            ``` json
            {
              "yaml.schemas": {
                "https://squidfunk.github.io/mkdocs-material/schema.json": "mkdocs.yml"
              }
            }
            ```

    === "Other"

        1.  Ensure your editor of choice has support for YAML schema validation.
        2.  Add the following lines at the top of `mkdocs.yml`:

            ``` yaml
            # yaml-language-server: $schema=https://squidfunk.github.io/mkdocs-material/schema.json
            ```

  [^1]:
    If you're a MkDocs plugin or Markdown extension author and your project
    works with Material for MkDocs, you're very much invited to contribute a
    schema for your [extension] or [plugin] as part of a pull request on GitHub.
    If you already have a schema defined, or wish to self-host your schema to
    reduce duplication, you can add it via [$ref].

  [configuration validation and auto-complete]: https://twitter.com/squidfunk/status/1487746003692400642
  [schema.json]: schema.json
  [vscode-yaml]: https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml
  [settings.json]: https://code.visualstudio.com/docs/getstarted/settings
  [extension]: https://github.com/squidfunk/mkdocs-material/tree/master/docs/schema/extensions
  [plugin]: https://github.com/squidfunk/mkdocs-material/tree/master/docs/schema/plugins
  [$ref]: https://json-schema.org/understanding-json-schema/structuring.html#ref

### 고급 설정 <a id="advanced-configuration"></a>
Material for MkDocs는 다양한 구성 옵션을 제공한다. 설정하기 섹션에서 색, 폰트, 아이콘 등을 설정하고 커스터마이즈하는 방법을 자세히 설명하고 있다.

<div class="mdx-columns" markdown>

- [Changing the colors]
- [Changing the fonts]
- [Changing the language]
- [Changing the logo and icons]
- [Ensuring data privacy]
- [Setting up navigation]
- [Setting up site search]
- [Setting up site analytics]
- [Setting up social cards]
- [Setting up tags]
- [Setting up versioning]
- [Setting up the header]
- [Setting up the footer]
- [Adding a git repository]
- [Adding a comment system]
- [Building for offline usage]

</div>

또한 전례가 없는 기술 문서 작성 경험을 제공하며 Material for MkDocs에 기본적으로 통합되어 지원되는 [Markdown extensions](https://squidfunk.github.io/mkdocs-material/setup/extensions/) 목록을 살펴보록 한다.

## 미리보기
MkDocs은 문서를 작성할 때 변경사항을 미리 볼 수 있는 라이브 미리보기 서버가 포함되어 있다. 서버는 저장 시 사이트를 자동으로 재구성란다. 

```
mkdocs serve
```
브라우저 주소창에 <code>localhost:8000</code>를 입력하면, 다음 화면을 볼 수 있다.

![first-screen](../images/fig-01.png)

## 사이트 구축
편집이 완료되면 Markdown 파일에서 다음 명령을 수행하여 정적 사이트를 만들 수 있다.

```
mkdocs build
```

이 디렉토리의 내용이 프로젝트 문서를 만든다. 데이터베이스나 서버가 내부적으로 장착되어 있기 때문에 따로 운영할 필요가 없다. 이 사이트는 [GitHub Pages](https://squidfunk.github.io/mkdocs-material/publishing-your-site/#github-pages), [GitLab Pages](https://squidfunk.github.io/mkdocs-material/publishing-your-site/#gitlab-pages), CDN 또는 개인 웹 공간에서 호스팅할 수 있다.
