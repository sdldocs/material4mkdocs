# Material4MkDocs

본 문서는 앞서 작성한 Technical Documentation을 위한 정적 웹 페이지 생성 프레임워크인 MkDocs의 테마인 MkDocs-material의 간단한 사용 설명서이다.
문서 파일은 Makrdown 파일 포맷으로 작성하여야 한다. 또한 이는 [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/), 특히 [시작하기](https://squidfunk.github.io/mkdocs-material/getting-started/)를 편역하였다.




